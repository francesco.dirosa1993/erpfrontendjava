import {RegisteredUser} from './registered-user';

export interface UserResponseApi {
  error : string;
  user : RegisteredUser;
}
