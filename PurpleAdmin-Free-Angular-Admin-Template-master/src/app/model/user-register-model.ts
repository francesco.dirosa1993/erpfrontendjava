export interface UserRegisterModel {
  name : string;
  lastname : string;
  username : string;
  password : string;
  email : string;
}
