import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BASEURL} from '../const/api-const';

const API_URL = `${BASEURL}/users`;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

}
