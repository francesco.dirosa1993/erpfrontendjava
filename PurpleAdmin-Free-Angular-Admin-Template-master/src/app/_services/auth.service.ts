import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BASEURL} from '../const/api-const';
import {UserRegisterModel} from '../model/user-register-model';
import {UserResponseApi} from '../model/user-response-api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any>{
    return this.http.post(`${BASEURL}/login`, {
      username,
      password
    });
  }

  register(registerUser : UserRegisterModel): Observable<UserResponseApi> {
    return this.http.post<UserResponseApi>(`${BASEURL}/users/sign-up`, registerUser);
  }
}
